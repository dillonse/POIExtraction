package com.csv;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class dateFormater{
	private static String Pattern="yyyy-MM-dd HH:mm:ss";
	private static SimpleDateFormat Format=new SimpleDateFormat(Pattern);
	private static java.util.Date date= new java.util.Date();
	private static Timestamp now;
	
	
	public static String formatCurrent(){return format(new Date());} 
	public static Date parse(String date) throws ParseException{return Format.parse(date);};
	public static String format(Date date){return Format.format(date);};
	public static Timestamp nowTimestamp(){now = new Timestamp(new Date().getTime());return now;}
}