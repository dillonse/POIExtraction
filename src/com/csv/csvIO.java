package com.csv;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class csvIO {

	static private List<String[]> list = new ArrayList<String[]>();

	static public List<String[]> getList(){return list;}
	
	public static  void select(String filename, String username,Date from,Date until) {
		list.clear();
		String csvPath = "./resources/csv/"+filename;
		BufferedReader buffer = null;
		String line = "";
		String csv_delimiter = "\t";
	
		try {
				buffer = new BufferedReader(new FileReader(csvPath));
				while ((line = buffer.readLine()) != null) {		//read line from csv
					String[] data = line.split(csv_delimiter); 	//split line to tokens
					if(data[1].equals(username)){
						Date date=null;
						date = dateFormater.parse(data[data.length-1]);
						if(date.before(until)&&date.after(from)){				//check if its our user and on the specified dates
							list.add(data);							//add the line to the list
						}
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}catch (ParseException e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
			} //parse the string to date//
			finally 
			{
				if (buffer != null) {
					try {
						buffer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			System.out.println("Done");
	}
}
