package com.clustering;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class StayPoints {
	
	static public double MaxDist=10;
	static public double MaxTime=10;
	static public double MinTime=1;
	
	
	static private class point_cart{
		public point_cart(double x,double y,double z){
			this.x=x;this.y=y;this.z=z;
		}
		public void add(point_cart p){
			x+=p.x;y+=p.y;z+=p.z;
		}
		public double x;
		public double y;
		public double z;
	}
	
	static public class stay_point{
		public stay_point(Coordinate c,Date start,Date end){
			this.center=c;this.start=start;this.end=end;
		}
		public Date start;
		public Date end;
		public Coordinate center;
	}
	
	static public Coordinate centroid(List<Coordinate> coords,List<Date> dates,int begin,int end){
		double lat;
		double lon;
		double hyp;
		double total_weight =0;
		point_cart point =new point_cart(0,0,0);
		//compute the midpoint
		for(int i=begin;i<end;i++){
			Date current=dates.get(i);
			total_weight+=current.getYear()*365.25+current.getMonth()*30.4375+current.getDay();
			point.add(WeightedPoint(coords.get(i),dates.get(i)));
		}
		point.x/=total_weight;point.y/=total_weight;point.z/=total_weight;
		//convert to lat long
		lon = Math.atan2(point.y,point.x)*(180/Math.PI);
		hyp = Math.sqrt(point.x*point.x+point.y*point.y);
		lat = Math.atan2(point.z,hyp)*(180/Math.PI);
		//return value
		return new Coordinate(lat,lon);
	}
	
	static private point_cart WeightedPoint(Coordinate c,Date d){
		//degrees to radians//
		double lat_rad = c.getLat()*(Math.PI/180f);
		double lon_rad = c.getLon()*(Math.PI/180f);
		//lat long to cartesians
		double x=Math.cos(lat_rad)*Math.cos(lon_rad);
		double y=Math.cos(lat_rad)*Math.sin(lon_rad);
		double z=Math.sin(lat_rad);
		//computation of weight
		double weight = (d.getYear())*365.25+(d.getMonth()*30.4375)+d.getDay();
		//adding the weight
		x*=weight;
		y*=weight;
		z*=weight;
		//return the new point
		return new point_cart(x,y,z);
	}
	
	static private double distance(Coordinate a,Coordinate b){
		double R = 6371000;
		double phi_a = a.getLat()*(Math.PI/180f);
		double phi_b = b.getLat()*(Math.PI/180f);
		double d_phi = (b.getLat() - a.getLat())*(Math.PI/180f);
		double d_lam = (b.getLon()-a.getLon())*(Math.PI/180f);
		double alpha = Math.sin(d_phi/2)*Math.sin(d_phi/2)+
				Math.cos(phi_a)*Math.cos(phi_b)*
				Math.sin(d_lam/2)*Math.sin(d_lam/2);
		double c = 2*Math.atan2(Math.sqrt(alpha),Math.sqrt(1-alpha));
		double d = R*c;
		return d;
	}
	
	static private double time_distance(Date a,Date b){
		return (a.getTime()-b.getTime())/1000;
	}
	
	static public List<stay_point> findStayPoints(List<Coordinate> coordinates,List<Date> dates){
		List<stay_point> result = new ArrayList<stay_point>();
		for(int i=0;i<coordinates.size();i++){
			for(int j=i+1;j<coordinates.size();j++){
				if(
						(distance(coordinates.get(i),coordinates.get(j))>MaxDist)||
						time_distance(dates.get(j),dates.get(i))<MinTime ||
						time_distance(dates.get(j),dates.get(i))>MaxTime)
				{
					result.add(new stay_point(centroid(coordinates,dates,i,j),dates.get(i),dates.get(j-1)));
					i=j;
				}
				else if(j==coordinates.size()-1){
					result.add(new stay_point(centroid(coordinates,dates,i,j+1),dates.get(i),dates.get(j)));
				}
			}
		}
		return result;
	}
	
	
	
}
