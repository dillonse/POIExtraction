package com.swing;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.clustering.StayPoints;
import com.clustering.StayPoints.stay_point;
import com.csv.*;
import com.toedter.calendar.JDateChooser;

import javax.swing.JTextField;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JMenuBar;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.DefaultMapController;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerCircle;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;
public class DDJFrame extends JFrame {

	private JPanel contentPane;
	private JTextField text_username;
	private JDateChooser dc_until;
	private JDateChooser dc_from;
	private JMapViewer map;
	private ChartPanel chart_panel;
	private JTextField text_dmax;
	private JTextField text_tmax;
	private JTextField text_tmin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DDJFrame frame = new DDJFrame();
					frame.setVisible(true);
					//frame.pack();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DDJFrame() {
		/***frame settings****/
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 801, 537);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		/****************/
		
		/**Text fields****/
		text_username = new JTextField();
		text_username.setFont(new Font("Hoefler Text", Font.PLAIN, 12));
		text_username.setForeground(Color.BLACK);
		text_username.setText("Username..");
		text_username.setBounds(75, 6, 61, 28);
		contentPane.add(text_username);
		text_username.setColumns(10);
		
		
		text_dmax = new JTextField();
		text_dmax.setBounds(471, 6, 51, 28);
		contentPane.add(text_dmax);
		text_dmax.setColumns(10);
		
		
		text_tmax = new JTextField();
		text_tmax.setBounds(583, 6, 51, 28);
		contentPane.add(text_tmax);
		text_tmax.setColumns(10);
		
		
		text_tmin = new JTextField();
		text_tmin.setBounds(695, 6, 51, 28);
		contentPane.add(text_tmin);
		text_tmin.setColumns(10);
		/****/
		
		/***dates chooser***/
		dc_from = new JDateChooser();
		dc_from.setBounds(208, 5, 69, 29);
		contentPane.add(dc_from);
		
		dc_until = new JDateChooser();
		dc_until.setBounds(338, 5, 68, 29);
		contentPane.add(dc_until);
		/************/
		
		/**labels**/
		JLabel lblFrom = new JLabel("From");
		lblFrom.setBounds(157, 3, 37, 29);
		contentPane.add(lblFrom);
		
		JLabel lblUntil = new JLabel("Until");
		lblUntil.setBounds(289, 3, 37, 29);
		contentPane.add(lblUntil);
		
		JLabel lblNewLabel = new JLabel("User");
		lblNewLabel.setBounds(26, 5, 37, 29);
		contentPane.add(lblNewLabel);
		
		JLabel lblTmin = new JLabel("Tmin");
		lblTmin.setBounds(646, 8, 37, 19);
		contentPane.add(lblTmin);
		
		JLabel lblDmax = new JLabel("Dmax");
		lblDmax.setBounds(418, 5, 61, 25);
		contentPane.add(lblDmax);
		
		JLabel lblTmax = new JLabel("Tmax");
		lblTmax.setBounds(534, 5, 37, 25);
		contentPane.add(lblTmax);
		
		/******/
		
		/**buttons**/
		JButton button_StayPoints = new JButton("Stay Points");
		 button_StayPoints.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//switch to map
				SwitchToMap();
				//get the list of lines 
				String username = text_username.getText();
				csvIO.select("gps.csv",username,dc_from.getDate(),dc_until.getDate());
				List<String[]> results = csvIO.getList();
				//create a list of dates and a list of coordinates
				List<Date> dates = new ArrayList<Date>();
				List<Coordinate> coords= new ArrayList<Coordinate>();
				List<stay_point> stay_points=null;
				for(int i=0;i<results.size();i++){
					String[] result= results.get(i);
					try {
						dates.add(dateFormater.parse(result[4])); //add the date
						Coordinate coord = new Coordinate(Double.parseDouble(result[2]),Double.parseDouble(result[3]));
						coords.add(coord); //add the coords
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				//calculate the staypoints//
				StayPoints.MaxDist =Double.parseDouble(text_dmax.getText());
				StayPoints.MaxTime = Double.parseDouble(text_tmax.getText());
				StayPoints.MinTime = Double.parseDouble(text_tmin.getText());
				stay_points=StayPoints.findStayPoints(coords,dates);
				updateMapStayPoints(stay_points);
				//add the marks on the map
				//UpdateMap(results,false,"StayPoints");
			}
		});
		button_StayPoints.setBounds(561, 448, 117, 29);
		contentPane.add(button_StayPoints);
		
		JButton button_accessPoints = new JButton("Access Points");
		button_accessPoints.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//switch to map
				SwitchToMap();
				//get the list of lines 
				String username = text_username.getText();
				csvIO.select("wifi.csv",username,dc_from.getDate(),dc_until.getDate());
				List<String[]> results = csvIO.getList();
				//add the marks on the map
				UpdateMap(results,false,"AccessPoints");
			}
		});
		button_accessPoints.setBounds(31, 448, 117, 29);
		contentPane.add(button_accessPoints);
		
		JButton button_path = new JButton("Path");
		button_path.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//switch to map
				SwitchToMap();
				String username = text_username.getText();
				csvIO.select("gps.csv",username,dc_from.getDate(),dc_until.getDate());
				List<String[]> results = csvIO.getList();
				UpdateMap(results,true,"Path");
			}
		});
		button_path.setBounds(160, 448, 117, 29);
		contentPane.add(button_path);
		
		JButton button_battery = new JButton("Battery");
		button_battery.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//switch to chart
				SwitchToChart();
				XYDataset ds = BatteryDataset();
				UpdateChart(ds);
			}
		});
		button_battery.setBounds(289, 448, 117, 29);
		contentPane.add(button_battery);
		
		JButton button_cells = new JButton("Cells");
		button_cells.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//switch to chart
				SwitchToMap();
				String username = text_username.getText();
				csvIO.select("base_station.csv",username,dc_from.getDate(),dc_until.getDate());
				List<String[]> results = csvIO.getList();
				UpdateMap(results,false,"Cells");
			}
		});
		button_cells.setBounds(418, 448, 117, 29);
		contentPane.add(button_cells);
		/**********/
		
		/*****Maps*****/
		map= new JMapViewer();
		DefaultMapController mapController = new DefaultMapController(map);
		map.setBounds(36,51,732,385);
		mapController.setMovementMouseButton(MouseEvent.BUTTON1);
		contentPane.add(map);
		/**************/
		
		/****Charts****/
		XYDataset ds = BatteryDataset();
		JFreeChart chart = ChartFactory.createXYLineChart("Battery","time(s)", "percent", ds, PlotOrientation.VERTICAL, true, true,false);
		chart_panel = new ChartPanel(chart);
		chart_panel.setBounds(map.getBounds());
		contentPane.add(chart_panel);
		
		/*************/
		
		SwitchToMap();
		
	}
	
	
	private void updateMapStayPoints(List<stay_point> stay_points){
		map.removeAllMapMarkers();
		map.removeAllMapPolygons();
		MapMarker mark = null;
		for(int i=0;i<stay_points.size();i++){
			stay_point point = stay_points.get(i);
			String label = point.center.getLat()+","+point.center.getLon()+":"+point.start+":"+point.end;
			mark = new MapMarkerDot(label,point.center);
			System.out.println("adding staypoint "+mark+toString());
			map.addMapMarker(mark);
		}
		System.out.println(StayPoints.MaxDist+":"+StayPoints.MaxTime+":"+StayPoints.MinTime);
		map.setDisplayToFitMapMarkers();
	}
	
	private void UpdateMap(List<String[]> results,boolean drawPath,String mode){
		//create the marks and the polylines
		map.removeAllMapMarkers();
		map.removeAllMapPolygons();
		Coordinate coord  = null;
		MapMarker mark = null;
		List<MapPolygon> polygon=null;
		List<Coordinate> mark_coords=new ArrayList<Coordinate>();
		for(int i=0;i<results.size();i++){
			String[] result = results.get(i);
			String lat = result[result.length-3];
			String lon = result[result.length-2];
			if(!lat.contains("No")&&!lon.contains("No")){
				coord = new Coordinate(Double.parseDouble(lat),Double.parseDouble(lon));
				String label="";
				if(mode == "AccessPoints")label=result[2]+":"+result[4]+":"+result[5];
				else if(mode=="Cells")label=result[2]+":"+result[3]+":"+result[4]+":"+result[5]+":"+result[6];
				mark = new MapMarkerDot(label,coord);
				System.out.println("adding mark "+mark+toString());
				map.addMapMarker(mark);
				mark_coords.add(coord);
			}
		}
		if(drawPath){
			polygon = PolygonToPolylines(mark_coords);
			for(int i=0;i<polygon.size();i++){
				map.addMapPolygon(polygon.get(i));
				System.out.println("added polygon to map"+polygon.get(i).toString());
			}
		}
		map.setDisplayToFitMapMarkers();
	}
	
	private void UpdateChart(XYDataset ds){
		JFreeChart chart = ChartFactory.createXYLineChart("Battery","time(s)", "percent", ds, PlotOrientation.VERTICAL, true, true,false);
		chart_panel.setChart(chart);
	}
	
	private List<MapPolygon> PolygonToPolylines(List<Coordinate> points){
		List<MapPolygon> list = new ArrayList<MapPolygon>();
		MapPolygon polygon =null;
		List<Coordinate> coords = null;
		for(int i=1;i<points.size();i++){
			coords=new ArrayList<Coordinate>();
			coords.add(points.get(i-1));
			coords.add(points.get(i));
			coords.add(points.get(i));
			polygon = new MapPolygonImpl(coords);
			list.add(polygon);
			System.out.println("added polyline "+ polygon.toString());
		}
		return list;
	}
	private XYDataset BatteryDataset() {
		//get the results//
        DefaultXYDataset ds = new DefaultXYDataset();
        csvIO.select("battery.csv",text_username.getText(),dc_from.getDate(), dc_until.getDate());
        List<String[]> results= csvIO.getList();
        //find the time amount from the first and last timestamp//
        double[][] data= new double[2][results.size()];
        if(results.size()>0){
        	String[] first = results.get(0);
        	Date fromDate=null,untilDate=null;
        	try {
				fromDate = dateFormater.parse(first[first.length-1]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	//get the time in seconds//
        	double timeoffset = fromDate.getTime();
        	for(int i=0;i<results.size();i++){
        		String[] result = results.get(i);
        		Date result_date=null;
				try {
					result_date = dateFormater.parse(result[result.length-1]);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
        		data[0][i] =((double)result_date.getTime() - timeoffset)/((1000));
        		data[1][i] =Long.parseLong(result[result.length-5]); 
        	}
        	
        }

        ds.addSeries("baterryTimeRatio", data);
        return ds;
    }
	
	private void SwitchToMap(){
		chart_panel.setVisible(false);
		map.setVisible(true);
	}
	
	private void SwitchToChart(){
		chart_panel.setVisible(true);
		map.setVisible(false);
	}
}
